import requests
import json


def send_message(tokens):
    url = "https://kapi.kakao.com/v2/api/talk/memo/default/send"

    headers = {
        "Authorization": 'Bearer ' + tokens["access_token"]
    }

    data = {"template_object": json.dumps(
        {
            "object_type": "list",
            "header_title": "Naver 정보제공",
            "header_text": "알림 메세지입니다.",
            "header_link": {
                "web_url": "https://www.naver.com",
                "mobile_web_url": "https://m.naver.com",
                "android_execution_params": "main",
                "ios_execution_params": "main"
            },
            "contents": [
                {
                    "title": "서울대벤처 맛집",
                    "description": "맛집 검색",
                    "image_url": "",
                    "image_width": "0", "image_height": "0",
                    "link": {
                        "web_url": "https://search.naver.com/search.naver?query=서울대벤처 맛집",
                        "mobile_web_url": "https://search.naver.com/search.naver?query=서울대벤처 맛집"
                    }
                },
                {
                    "title": "서울대벤처 카페",
                    "description": "카페 검색",
                    "image_url": "",
                    "image_width": "0", "image_height": "0",
                    "link": {
                        "web_url": "https://search.naver.com/search.naver?query=서울대벤처 카페",
                        "mobile_web_url": "https://search.naver.com/search.naver?query=서울대벤처 카페"
                    }
                }
            ],
            "button_title": "Naver로 이동하기"
        }
    )}

    response = requests.post(url, headers=headers, data=data)

    if response.status_code == 200:
        return response.status_code
    else:
        return response.status_code

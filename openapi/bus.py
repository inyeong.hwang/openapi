import requests
from json import JSONDecodeError


# 공공데이터포털: 경기도_정류장 주변도로 미세먼지빅데이터 기반 대응시스템 공공데이터 조회 OpenAPI
# url: 공공데이터포털도메인/API를 오픈한 기관코드/API서비스명/API오퍼레이션명
def get_dust_info(params):
    url = 'http://apis.data.go.kr/6410000/GOA/GOA002'
    response = requests.get(url, params=params)
    try:
        data = response.json()['result']['data']
        return data
    except TypeError:
        raise JSONDecodeError("유효하지 않은 파라미터임!")

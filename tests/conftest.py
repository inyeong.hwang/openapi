import json
import os
import pytest
from pytest import fixture


def findfile(name, path):
    for dirpath, dirname, filename in os.walk(path):
        if name in filename:
            return os.path.join(dirpath, name)


@pytest.fixture
def token_load():
    fail_fp = findfile("kakao_ftoken.json", "/")
    success_fp = findfile("kakao_stoken.json", "/")
    with open(fail_fp, "r") as fp:
        fail_tokens = json.load(fp)
    with open(success_fp, "r") as fp:
        success_tokens = json.load(fp)
    return fail_tokens, success_tokens


@pytest.fixture
def test_params():
    success_params = {
        'serviceKey': 'NDjbNjs6XoxIcQo7/BOZwbMtaBNKFjZhfHpA2E7YAQcdqDi4GGAdnM94fSwje8hM4OYZx/MQMSWJt2uGurFeeQ==',
        'type': 'json',
        'numOfRows': '10',
        'pageNo': '1',
        'busno': '23537'
    }
    fail_params = {
        'type': 'json',
        'numOfRows': '10',
        'pageNo': '1',
        'busno': '23537'
    }
    return success_params, fail_params

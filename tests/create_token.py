# # 인증코드 생성 url
# # https://kauth.kakao.com/oauth/authorize?response_type=code&client_id=585a432bda116f35f8933d2c367651fb&redirect_uri=https://naver.com&scope=talk_message


import requests
import json

# 토큰 발급, 저장
url = 'https://kauth.kakao.com/oauth/token'
rest_api_key = '585a432bda116f35f8933d2c367651fb'
redirect_uri = 'https://naver.com'
authorize_code = '5lKVhxW0xMoK5QPGqQZJg6OEAc0p8JYPy7epASBS3S5W5a_yWUcssMn-Dw1EGNbnTJ-gLQo9dGkAAAGGFkis5g'

data = {
    'grant_type': 'authorization_code',
    'client_id': rest_api_key,
    'redirect_uri': redirect_uri,
    'code': authorize_code,
}

response = requests.post(url, data=data)
tokens = response.json()
print(tokens)


with open("C:\\Users\\iyhwang\\PycharmProjects\\openAPIProject\\venv\\tests\\kakao_stoken.json", "w") as fp:
    json.dump(tokens, fp)

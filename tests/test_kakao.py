from openapi import kakao


def test_send_message(token_load):
    fail_tokens, success_tokens = token_load
    assert kakao.send_message(fail_tokens) == 401
    assert kakao.send_message(success_tokens) == 200

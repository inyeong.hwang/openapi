# import pytest
# from openapi import bus
# from json import JSONDecodeError
#
#
# def test_get_dust_info(test_params):
#     success_params, fail_params = test_params
#     assert all([x in bus.get_dust_info(success_params) for x in ['mTime', 'pm2.5', 'pm10', 'co', 'so2', 'no2', 'temp',
#                                                                  'hum', 'wd', 'ws', 'status']])
#     with pytest.raises(JSONDecodeError):
#         assert bus.get_dust_info(fail_params)
